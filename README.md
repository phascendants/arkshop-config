# ArkShop Config
you need to change every config.json file in every map
config location:
```
                                        Map Name
                                            |
C:\ARK_ASM\ASM_Data\Vanilla\Servers\Vanilla_TheIsland\ShooterGame\Binaries\Win64\ArkApi\Plugins\ArkShop
                      |
                Cluster Name
```

For web config location
```																		
                                                                     Router
                                                                       |
C:\Users\Administrator\Documents\PH Ascendants\arkshop\arkshop_config\qol\config.json

Change router
open C:\Users\Administrator\Documents\PH Ascendants\arkshop\main.js

        Router Name	                 Router
           |		                   |
const QoLROuter = require('./router/QoLRouter'); you find this at C:\Users\Administrator\Documents\PH Ascendants\arkshop\router\QoLRouter.js

	Router Location -> localhost/qol/
           |
app.use("/qol", QoLROuter);
```

Add item for sale
```
"Kits": { <- loot for this
      "Rex": { <- dino name at shop
        "DefaultAmount": 0, <- quantity gives to new player 
        "Price": 500,
        "Description": "No description.",
        "Dinos": [ 
          {
            "Level": 1, 
            "Gender": "random", <- gender 3 types "random, male, female"
            "Blueprint": "Blueprint'/Game/PrimalEarth/Dinos/Rex/Rex_Character_BP.Rex_Character_BP'" <- Dino BP
          }
        ]
      },
      "TekHelmet": { <- item name 
        "DefaultAmount": 0, <- quantity gives to new player 
        "Price": 300, 
        "Description": "No description.", 
        "Items": [
          {
            "Amount": 1, <- quantity of the receive by player when purchase
            "Quality": 65, <- quality of item 65 to make the item ascendant 
            "Armor": 714.6, <- if item is player armor or saddle that use armor you can set both damage and armor in some cases like andro saddle
            "Durability": 10000, <- optional this is for player armor, weapon and tools not for saddle
            "ForceBlueprint": false, <- if u want to make it blue print or not
            "Blueprint": "Blueprint'/Game/PrimalEarth/CoreBlueprints/Items/Armor/TEK/PrimalItemArmor_TekHelmet.PrimalItemArmor_TekHelmet'" <- Item BP
          }
        ]
      },
      "Chainsaw": { <- item name 
        "DefaultAmount": 0, <- quantity gives to new player 
        "Price": 300,
        "Description": "No description.",
        "Items": [
          {
            "Amount": 1, <- quantity of the receive by player when purchase
            "Quality": 65, <- quality of item 65 to make the item ascendant 
            "Damage": 298, <- if item is player weapon, tools or saddle that use damage you can set both damage and armor in some cases like andro saddle
            "Durability": 10000, <- optional this is for player armor, weapon and tools not for saddle
            "ForceBlueprint": false, <- if u want to make it blue print or not
            "Blueprint": "Blueprint'/Game/ScorchedEarth/WeaponChainsaw/PrimalItem_ChainSaw.PrimalItem_ChainSaw'" ITEM BP
          }
        ]
      },
    },
```
Add exchange item
```
"SellItems": { <- look for this
        "metal": { <- exchange name
            "Type": "item", <- sell type
            "Description": "100x stone", <- description
            "Price": 10, <- how much points receive by player
            "Amount": 100, <- quantity of items you need to exchange
            "Blueprint": "Blueprint'/Game/Mods/Stack50/Resources/PrimalItemResource_Stone_Child.PrimalItemResource_Stone_Child'" <- Item BP
        } 
    },
```
